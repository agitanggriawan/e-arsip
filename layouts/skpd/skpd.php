<div class="row">
  <form class="formValidate" id="validateSkpdPengusul">
    <div class="col s12 m12 l12">
      <div class="card">
        <div class="card-content">
          <span class="card-title">SKPD Pengusul (PA - KPA)</span>
          <div class="row">
            <div class="input-field col s6">
              <label for="namaSkpdUser">Nama Pengguna SKPD</label>
              <input id="namaSkpdUser" name="namaSkpdUser" type="text" data-error=".errorNamaSkpdUser" disabled value="<?php echo base64_decode($_SESSION['nama_pegawai']) ?>">
              <div class="errorNamaSkpdUser errorText"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
<div class="row">
  <form class="formValidate" id="validateUsulanSebelum">
    <div class="col s12 m6 l6">
      <div class="card">
        <div class="card-content">
          <span class="card-title">Usulan Sebelum</span>
          <div class="row">
            <div class="input-field col s11 m5 l11">
              <label id="namaPenyelia">Kegiatan</label>
              <div id="selectPenyelia">
                <select name="kegiatan" class="select2Basic select-sebelum">
                  <option selected disabled>--- Pilih ---</option>
                  <?php echo $md->getKegiatan(); ?>
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s11 m5 l11">
              <label id="namaPenyelia">Pegawai</label>
              <div id="selectPenyelia">
                <select name="penyelia" class="select2Basic select-menjadi select-penyelia-sebelum">
                  <option selected disabled>--- Pilih ---</option>
                  <?php echo $md->getPenyeliaByDinas(base64_decode($_SESSION['nama_pegawai'])); ?>
                </select>
              </div>
            </div>
          </div>
          <div class="row text-usulan">
            <div class="input-field col s12">
              <label for="skpd">SKPD Sebelum</label>
              <input id="skpd" name="skpd" type="text" data-error=".errorSkpd" readonly value=" ">
              <div class="errorSkpd errorText"></div>
            </div>
          </div>
          <div class="row text-usulan">
            <div class="input-field col s12">
              <label for="jabatanSebelum"> Jabatan Sebelum</label>
              <input id="jabatanSebelum" name="jabatanSebelum" type="text" data-error=".errorJabatanSebelum" readonly value=" ">
              <div class="errorJabatanSebelum errorText"></div>
            </div>
          </div>
          <div class="row text-usulan">
            <div class="input-field col s12">
              <label for="otoritasSebelum">Otoritas Sebelum</label>
              <input id="otoritasSebelum" name="otoritasSebelum" type="text" data-error=".errorOtoritasSebelum" readonly value=" ">
              <div class="errorOtoritasSebelum errorText"></div>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s11 m5 l11 text-tahapan-sebelum">
              <label id="namaPenyelia">Tahapan</label>
              <div id="selectPenyelia">
                <select name="tahapan" class="select2Basic select-sebelum">
                  <option selected disabled>--- Pilih ---</option>
                  <?php echo $md->getTahapan(); ?>
                </select>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>
  <form class="formValidate" id="validateUsulanMenjadi">
    <div class="col s12 m6 l6">
      <div class="card">
        <div class="card-content">
          <span class="card-title">Usulan Menjadi</span>
          <div class="row">
            <div class="input-field col s11 m5 l11">
              <label id="namaPenyelia">Kegiatan</label>
              <div id="selectPenyelia">
                <select name="kegiatan" class="select2Basic select-menjadi">
                  <option selected disabled>--- Pilih ---</option>
                  <?php echo $md->getKegiatan(); ?>
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s11 m5 l11">
              <label id="namaPenyelia">Pegawai</label>
              <div id="selectPenyelia">
                <select name="penyelia" class="select2Basic select-menjadi">
                  <option selected disabled>--- Pilih ---</option>
                  <?php echo $md->getPenyeliaWithId(); ?>
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s11 m5 l11">
              <label id="namaPenyelia">SKPD Menjadi</label>
              <div id="selectPenyelia">
                <select name="skpd" class="select2Basic select-menjadi">
                  <option selected disabled>--- Pilih ---</option>
                  <?php echo $md->getSkpd(); ?>
                </select>
              </div>
            </div>
          </div>
          <div class="row text-usulan">
            <div class="input-field col s12">
              <label for="jabatanMenjadi">Jabatan Menjadi</label>
              <input id="jabatanMenjadi" name="jabatanMenjadi" type="text" data-error=".errorJabatanMenjadi">
              <div class="errorJabatanMenjadi errorText"></div>
            </div>
          </div>
          <div class="row input-otoritas-usulan">
            <div class="input-field col s12 m5 l12">
              <select id="otoritasMenjadi" name="otoritasMenjadi" multiple class="select_all select select-otoritas-usulan" required="" aria-required="true" title="Pilih otoritas">
                <option value="" disabled>--- Otoritas ---</option>
                <option value="KPA">1. Kuasa Pengguna Anggaran</option>
                <option value="PA">2. Pengguna Anggaran</option>
                <option value="Penyelia">3. Penyelia</option>
                <option value="Super Admin">4. Super Admin</option>
              </select>
              <label for="otoritas">Otoritas</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s11 m5 l11">
              <label id="namaPenyelia">Tahapan</label>
              <div id="selectPenyelia">
                <select name="tahapan" class="select2Basic select-menjadi">
                  <option selected disabled>--- Pilih ---</option>
                  <?php echo $md->getTahapan(); ?>
                </select>
              </div>
            </div>
          </div>
          <div class="row input-file-usulan">
            <div class="file-field input-field">
              <div class="btn">
                <span>File</span>
                <input type="file" class="file-usulan">
              </div>
              <div class="file-path-wrapper">
                <input class="file-path validate" type="text" placeholder="File usulan">
              </div>
            </div>
          </div>
          <div class="row">
            <button class="btn waves-effect waves-light blue pengusul-skpd-button" type="submit">Ajukan</button>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>