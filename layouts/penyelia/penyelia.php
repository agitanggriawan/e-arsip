<div class="row">
  <div class="col s12 m12 l12">
    <div class="card">
      <div class="card-content">
        <div class="material-table">
          <div class="table-header">
            <span class="card-title">Pencarian</span>
            <div class="actions">
              <a href="Javascript:void(0)" class="search-toggle waves-effect btn-flat"><i class="material-icons">search</i></a>
            </div>
          </div>
          <table id="table-usulan-skpd" class="bordered highlight">
            <thead>
              <tr>
                <th width="100px">No.</th>
                <th>SKPD Pengusul</th>
                <th>Kode & Kegiatan</th>
                <th>Tahapan</th>
                <th width="250px">Sebelum Usulan</th>
                <th width="250px">Usulan Menjadi</th>
                <th width="75px">File</th>
                <th>Tanggal Pengajuan</th>
                <th>Status</th>
                <th>Catatan Penyelia</th>
                <th>Catatan SKPD</th>
                <th>Aksi</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>