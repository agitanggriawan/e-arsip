<div class="row">
  <div class="col s12 m12 l12">
    <div class="card">
      <div class="card-content">
        <!-- <div class="row">
          <div class="input-field col s3">
            <label id="namaPenyelia">Nama Penyelia</label>
            <div id="selectPenyelia">
              <select id="list-skpd" name="skpd" class="select2Basic">
                <option selected disabled>--- Pilih ---</option>
                <option>Semua</option>
                <?php echo $md->getPenyelia(); ?>
              </select>
            </div>
          </div>
        </div> -->
        <div class="material-table">
          <div class="table-header">
            <!-- <div class="row"> -->
              <div class="input-field col s6 filer-name">
                <label id="namaPenyelia">Nama Penyelia</label>
                <div id="selectPenyelia">
                  <select id="list-skpd" name="skpd" class="select2Basic">
                    <option selected disabled>--- Pilih ---</option>
                    <option>Semua</option>
                    <?php echo $md->getPenyelia(); ?>
                  </select>
                </div>
              </div>
            <!-- </div> -->
            <div class="actions">
              <a href="Javascript:void(0)" class="search-toggle waves-effect btn-flat"><i class="material-icons">search</i></a>
            </div>
          </div>
          <!-- <table border="0" cellspacing="5" cellpadding="5">
            <tbody>
              <tr>
                <td>Minimum age:</td>
                <td><input type="text" id="min" name="min"></td>
              </tr>
            </tbody>
          </table> -->
          <table id="table-monitoring-skpd" class="bordered highlight">
            <thead>
              <tr>
                <th width="100px">No.</th>
                <th>SKPD Pengusul</th>
                <th>Kode & Kegiatan</th>
                <!-- <th>Nama Penyelia</th> -->
                <th>Tahapan</th>
                <th width="250px">Sebelum Usulan</th>
                <th width="250px">Usulan Menjadi</th>
                <th>Tanggal Pengajuan</th>
                <th>Tanggal Verifikasi</th>
                <th>Status</th>
                <th>Catatan Penyelia</th>
                <th>Catatan SKPD</th>
                <th width="75px">File</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>