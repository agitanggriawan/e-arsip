<div class="row">
  <form class="formValidate" id="validateKegiatan">
    <div class="col s12 m12 l12">
      <div class="card">
        <div class="card-content">
          <span class="card-title">Jenis Kegiatan</span>
          <div class="row">
            <div class="input-field col s3" style="width: 320px !important; margin-right: 20px">
                <label style="z-index: 100; position: initial; font-size: 12px" for="skpd">SKDP</label>
                <select id="skpd" name="skpd" class="select2Basic select-skpd" required="" aria-required="true" title="Pilih skpd">
                  <option value="" disabled selected>--- SKPD ---</option>
                  <?php echo $md->getSkpd(); ?>
                  <!-- <option value="Dinas">Dinas</option>
                  <option value="Lainnya">Lainnya</option> -->
                </select>
              </div>
            <div class="input-field col s4">
              <label for="kodeKegiatan">Kode Kegiatan</label>
              <input id="kodeKegiatan" name="kodeKegiatan" type="text" data-error=".errorKodeKegiatan">
              <div class="errorKodeKegiatan errorText"></div>
            </div>
            <div class="input-field col s4">
              <label for="namaKegiatan">Nama Kegiatan</label>
              <input id="namaKegiatan" name="namaKegiatan" type="text" data-error=".errorNamaKegiatan">
              <div class="errorNamaKegiatan errorText"></div>
            </div>
          </div>
        </div>
        <div class="card-action">
          <button class="btn waves-effect waves-light blue kegiatan-button" type="submit">Simpan</button>
          <button class="btn waves-effect waves-light white kegiatan-button black-text" type="reset">Reset</button>
        </div>
      </div>
    </div>
  </form>
</div>
<div class="row">
  <div class="col s12 m12 l12">
    <div class="card">
      <div class="card-content">
        <!-- <span class="card-title">Pencarian</span> -->
        <div class="material-table">
          <div class="table-header">
            <span class="card-title">Pencarian</span>
            <div class="actions">
              <a href="Javascript:void(0)" class="search-toggle waves-effect btn-flat"><i class="material-icons">search</i></a>
            </div>
          </div>
          <table id="table-kegiatan" class="bordered highlight">
            <thead>
              <tr>
                <th width="30px">No.</th>
                <th width="100px">Kode Kegiatan</th>
                <th width="100px">SKPD</th>
                <th width="100px">Nama Kegiatan</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>