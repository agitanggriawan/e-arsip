<div class="row">
  <div class="col s12 m12 l12">
    <div class="card card-nav">
      <div class="card-content content-nav">
        <div class="row">
          <div class="col s12">
            <ul class="tabs">
              <li class="tab col s3"><a class="active" href="#input">Set Penyelia</a></li>
              <li class="tab col s3"><a href="#master">Data</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row" id="input">
  <div class="col s12 m12 l12">
    <div class="card">
      <div class="card-content">
        <div class="row">
          <div class="input-field col s3">
            <label id="namaPenyelia">Nama Penyelia</label>
            <div id="selectPenyelia">
              <select name="skpd" class="select2Basic select-penyelia">
                <option selected disabled>--- Pilih ---</option>
                <?php echo $md->getPenyelia(); ?>
              </select>
            </div>
          </div>
        </div>
        <div class="material-table">
          <div class="table-header">
            <!-- <span class="card-title">Pencarian</span> -->
            <button class="btn waves-effect waves-light blue penyelia-button" id="set-penyelia" type="submit" style="margin-left: -14px">Terapkan</button>

            <div class="actions">
              <a href="Javascript:void(0)" class="search-toggle waves-effect btn-flat"><i class="material-icons">search</i></a>
            </div>
          </div>
          <form id="myform">
            <table id="table-skpd" class="bordered highlight">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Dinas</th>
                  <th>Aksi</th>
                </tr>
              </thead>
            </table>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row" id="master">
<div class="col s12 m12 l12">
    <div class="card">
      <div class="card-content">
        <div class="material-table">
          <div class="table-header">
            <div class="actions">
              <a href="Javascript:void(0)" class="search-toggle waves-effect btn-flat"><i class="material-icons">search</i></a>
            </div>
          </div>
          <table id="table-checklist-skpd" class="bordered highlight">
            <thead>
              <tr>
                <th>No</th>
                <th>NIP</th>
                <th>Nama Pegawai</th>
                <th>Aksi</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>