<div class="row">
  <form class="formValidate" id="validateTahapan">
    <div class="col s12 m12 l12">
      <div class="card">
        <div class="card-content">
          <span class="card-title">Tahapan</span>
          <div class="row">
            <div class="input-field col s6">
              <label for="namaTahapan">Nama Tahapan</label>
              <input id="namaTahapan" name="namaTahapan" type="text" data-error=".errorNamaTahapan">
              <div class="errorNamaTahapan errorText"></div>
            </div>
          </div>
        </div>
        <div class="card-action">
          <button class="btn waves-effect waves-light blue tahapan-button" type="submit">Simpan</button>
          <button class="btn waves-effect waves-light white tahapan-button black-text" type="reset">Reset</button>
        </div>
      </div>
    </div>
  </form>
</div>
<div class="row">
  <div class="col s12 m12 l12">
    <div class="card">
      <div class="card-content">
        <div class="material-table">
          <div class="table-header">
            <span class="card-title">Pencarian</span>
            <div class="actions">
              <a href="Javascript:void(0)" class="search-toggle waves-effect btn-flat"><i class="material-icons">search</i></a>
            </div>
          </div>
          <table id="table-tahapan" class="bordered highlight">
            <thead>
              <tr>
                <th width="30px">No.</th>
                <th width="30px">ID</th>
                <th>Nama Tahapan</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>