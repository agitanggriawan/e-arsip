<div class="container-fluid">
  <!-- <div class="row">
    <div class="input-field col s3">
      I`m Pegawai Page
      <select class="select2Basic" name="state" width="200px">
        <option value="AL">Alabama</option>
        <option value="AL">Alabama</option>
        <option value="AL">Alabama</option>
        <option value="WY">Wyoming</option>
      </select>
    </div>
  </div> -->
  <div class="row">
    <form class="formValidate" id="validateProfile">
      <div class="col s12 m12 l12">
        <div class="card">
          <div class="card-content">
            <span class="card-title">Profil Pegawai</span>
            <div class="row">
              <div class="input-field col s6">
                <label for="namaLengkap">Nama Lengkap</label>
                <input id="namaLengkap" name="namaLengkap" type="text" data-error=".errorNamaLengkap">
                <div class="errorNamaLengkap errorText"></div>
              </div>
              <div class="input-field col s6">
                <label for="nip">Nomor Induk Pegawai</label>
                <input id="nip" name="nip" type="text" data-error=".errorNip">
                <div class="errorNip errorText"></div>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s4">
                <label for="jabatan">Jabatan</label>
                <input id="jabatan" name="jabatan" type="text" data-error=".errorJabatan">
                <div class="errorJabatan errorText"></div>
              </div>
              <div class="input-field col s4">
                <label for="password">Kata Sandi</label>
                <input id="password" name="password" type="password" data-error=".errorPassword">
                <div class="errorPassword errorText"></div>
              </div>
              <div class="input-field col s4">
                <label for="passwordConfirm">Konfirmasi Kata Sandi</label>
                <input id="passwordConfirm" name="passwordConfirm" type="password" data-error=".errorPasswordConfirm">
                <div class="errorPasswordConfirm errorText"></div>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s5">
              <label style="z-index: 100; position: initial; font-size: 12px" for="skpd">SKDP</label>
                <select id="skpd" name="skpd" class="select2Basic" required="" aria-required="true" title="Pilih skpd">
                  <option value="" disabled selected>--- SKPD ---</option>
                  <?php echo $md->getSkpd(); ?>
                  <!-- <option value="Dinas">Dinas</option>
                  <option value="Lainnya">Lainnya</option> -->
                </select>
              </div>
              <div class="input-field col s1"></div>
              <div class="input-field col s6">
                <select id="otoritas" name="otoritas" multiple class="select_all select" required="" aria-required="true" title="Pilih otoritas">
                  <option value="" disabled>--- Otoritas ---</option>
                  <option value="KPA">1. Kuasa Pengguna Anggaran</option>
                  <option value="PA">2. Pengguna Anggaran</option>
                  <option value="Penyelia">3. Penyelia</option>
                  <option value="Super Admin">4. Super Admin</option>
                </select>
                <label for="otoritas">Otoritas</label>
              </div>
            </div>
          </div>
          <div class="card-action">
            <button class="btn waves-effect waves-light blue profile-button" type="submit">Simpan</button>
            <button class="btn waves-effect waves-light white profile-button black-text" type="reset">Reset</button>
          </div>
        </div>
      </div>
    </form>
  </div>
  <div class="row">
    <div class="col s12 m12 l12">
      <div class="card">
        <div class="card-content">
          <!-- <span class="card-title">Pencarian</span> -->
          <div class="material-table">
            <div class="table-header">
              <span class="card-title">Pencarian</span>
              <div class="actions">
                <a href="Javascript:void(0)" class="export-in-csv waves-effect tooltipped   btn-flat" data-position="bottom" data-delay="50" data-tooltip="Export in CSV"><i class="material-icons">content_copy</i></a>
                <a href="Javascript:void(0)" class="search-toggle waves-effect btn-flat"><i class="material-icons">search</i></a>
            </div>
            </div>
            <table id="table-pegawai" class="bordered highlight">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Nomor Induk Pegawai</th>
                  <th>Nama Pegawai</th>
                  <th>Jabatan</th>
                  <th>SKPD</th>
                  <th>Otoritas</th>
                  <!-- <th>Aksi</th> -->
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
