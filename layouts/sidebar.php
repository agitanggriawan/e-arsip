<nav>
  <a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons">menu</i></a>
  <div class="titleNav"><?php echo $md->checkTitle($_GET['module']); ?></div>
</nav>
<ul id="slide-out" class="sidenav sidenav-fixed">
  <li>
    <div class="user-view">
      <div class="background" style="background-color: #42a5f5"></div>
      <a href="#user"><img class="circle" src="https://img.icons8.com/bubbles/2x/administrator-male.png"></a>
      <a href="#"><span class="white-text name"><?php echo base64_decode($_SESSION['nama_pegawai']) ?></span></a>
      <a href="#"><span class="white-text email"><?php echo @end(json_decode($_SESSION['id_otoritas'])) . ' | ' . base64_decode($_SESSION['nip']) ?></span></a>
    </div>
  </li>
  <?php
  if (in_array("Super Admin", (json_decode($_SESSION['id_otoritas'])))) {
  ?>
  <li><a class="waves-effect" href="index?module=pegawai">Pegawai</a></li>
  <li><a class="waves-effect" href="index?module=kegiatan">Kegiatan</a></li>
  <li><a class="waves-effect" href="index?module=tahapan">Tahapan</a></li>
  <!-- <li><a class="waves-effect" href="index?module=penyelia">Penyelia</a></li> -->
  <!-- <li><a class="waves-effect" href="index?module=skpd">SKPD Pengusul</a></li> -->
  <li><a class="waves-effect" href="index?module=set-penyelia">Set Penyelia</a></li>
  <li><a class="waves-effect" href="index?module=monitoring">Monitoring</a></li>
  <!-- <li><a class="waves-effect" href="index?module=history">History</a></li> -->
  <?php
  }
  ?>

  <?php
  if (in_array("Super Admin", (json_decode($_SESSION['id_otoritas']))) || in_array("KPA", (json_decode($_SESSION['id_otoritas']))) || in_array("PA", (json_decode($_SESSION['id_otoritas']))) || in_array("Admin", (json_decode($_SESSION['id_otoritas'])))) {
  ?>
  <li><a class="waves-effect" href="index?module=skpd">SKPD Pengusul</a></li>
  <li><a class="waves-effect" href="index?module=data-skpd">Data SKPD Pengusul</a></li>
  <?php
  }
  ?>

<?php
  if (in_array("Super Admin", (json_decode($_SESSION['id_otoritas']))) || (strpos($_SESSION['id_otoritas'], 'Penyelia') !== false) ) {
  ?>
  <li><a class="waves-effect" href="index?module=penyelia">Penyelia</a></li>
  <?php
  }
  ?>
  <li><a class="waves-effect" href="index?module=logout">Logout</a></li>
</ul>