<?php
  // Need to check authentication by role user
  if (isset($_GET['module'])) {
    if (in_array("Super Admin", (json_decode($_SESSION['id_otoritas'])))) {
      if ($_GET['module'] == 'pegawai') {
        include 'layouts/super-admin/pegawai.php';
      } else if ($_GET['module'] == 'kegiatan') {
        include 'layouts/super-admin/kegiatan.php';
      } else if ($_GET['module'] == 'tahapan') {
        include 'layouts/super-admin/tahapan.php';
      } else if ($_GET['module'] == 'set-penyelia') {
        include 'layouts/super-admin/set-penyelia.php';
      } else if ($_GET['module'] == 'monitoring') {
        include 'layouts/super-admin/monitoring.php';
      } else if ($_GET['module'] == 'logout') {
        include 'helper/logout.php';
      } else if ($_GET['module'] == 'skpd') {
        include 'layouts/skpd/skpd.php';
      } else if ($_GET['module'] == 'penyelia') {
        include 'layouts/penyelia/penyelia.php';
      } else {
        include 'layouts/404.php';
      }
    } else if (in_array("Super Admin", (json_decode($_SESSION['id_otoritas']))) || in_array("KPA", (json_decode($_SESSION['id_otoritas']))) || in_array("PA", (json_decode($_SESSION['id_otoritas']))) || in_array("Admin", (json_decode($_SESSION['id_otoritas'])))) {
      if ($_GET['module'] == 'skpd') {
        include 'layouts/skpd/skpd.php';
      } else if ($_GET['module'] == 'data-skpd') {
        include 'layouts/skpd/data-skpd.php';
      } else if ($_GET['module'] == 'logout') {
        include 'helper/logout.php';
      } else {
        include 'layouts/404.php';
      }
    } else if (strpos($_SESSION['id_otoritas'], 'Penyelia') !== false) {
      if ($_GET['module'] == 'penyelia') {
        include 'layouts/penyelia/penyelia.php';
      } else if ($_GET['module'] == 'logout') {
        include 'helper/logout.php';
      } else {
        include 'layouts/404.php';
      }
    } else {
      include 'layouts/404.php';
    }
  }
?>