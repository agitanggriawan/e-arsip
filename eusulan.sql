/*==============================================================*/
/* DBMS name:      Sybase SQL Anywhere 12                       */
/* Created on:     25/05/2019 15.14.16                          */
/*==============================================================*/


drop table if exists histori;

drop table if exists jenis_kegiatan;

drop table if exists setting_penyelia;

drop table if exists skpd;

drop table if exists otoritas;

drop table if exists pegawai;

drop table if exists pengajuan;

drop table if exists status;

drop table if exists tahapan;

drop table if exists update_penyelia;



/*==============================================================*/
/* Table: HISTORI                                               */
/*==============================================================*/
create table HISTORI 
(
   ID_HISTORI           int                        not null,
   ID_otoritas          long varchar                   null,
   ID                   long varchar                   null,
   NAMA_TABEL           long varchar                   null,
   OLD_VALUE            long varchar                   null,
   NEW_VALUE            long varchar                   null,
   created_at           datetime DEFAULT CURRENT_TIMESTAMP,
   updated_at           datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   constraint pk_HISTORI primary key clustered (ID_HISTORI)
);

/*==============================================================*/
/* Table: jenis_kegiatan                                        */
/*==============================================================*/
create table jenis_kegiatan 
(
   id_jenis_kegiatan    int                            not null,
   kode_kegiatan        varchar(15)                    null,
   nama_kegiatan        varchar(50)                    null,
   created_at           datetime DEFAULT CURRENT_TIMESTAMP,
   updated_at           datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   constraint pk_jenis_kegiatan primary key clustered (id_jenis_kegiatan)
);

/*==============================================================*/
/* Table: skpd                                                  */
/*==============================================================*/
create table skpd 
(
   id_skpd              int                            not null,
   nama_skpd            varchar(100)                   null,
   created_at           datetime DEFAULT CURRENT_TIMESTAMP,
   updated_at           datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   constraint pk_skpd primary key clustered (id_skpd)
);

/*==============================================================*/
/* Table: otoritas                                              */
/*==============================================================*/
create table otoritas 
(
   id_otoritas          int                            not null,
   nama_otoritas        varchar(50)                    not null,
   created_at           datetime DEFAULT CURRENT_TIMESTAMP,
   updated_at           datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   constraint pk_otoritas primary key clustered (id_otoritas)
);

/*==============================================================*/
/* Table: pegawai                                               */
/*==============================================================*/
create table pegawai 
(
   id_pegawai           int                     not null,
   id_otoritas          text                    not null,
   nip_pegawai          varchar(20)             null,
   nama_pegawai         varchar(50)             null,
   jabatan_sebelum      varchar(50)             null,
   skpd_sebelum         varchar(50)             null,
   password             text                    null,
   last_login           datetime                null,
   active               tinyint DEFAULT 0                null,
   status               tinyint DEFAULT 1                null,
   created_at           datetime DEFAULT CURRENT_TIMESTAMP,
   updated_at           datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   constraint pk_pegawai primary key clustered (ID_pegawai)
);

/*==============================================================*/
/* Table: pengajuan                                             */
/*==============================================================*/
create table pengajuan 
(
   id_pengajuan          int                     not null,
   usulan_sebelum        text                    null,
   usulan_menjadi        text                    null,
   status                text                    null,
   tanggal_pengajuan     datetime                null,
   tanggal_verifikasi    datetime                null,
   note_penyelia         text                    null,
   note_skpd             text                    null,
   file                  text                    null,
   created_at            datetime DEFAULT CURRENT_TIMESTAMP,
   updated_at            datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   constraint pk_pengajuan primary key clustered (id_pengajuan)
);

/*==============================================================*/
/* Table: STATUS                                                */
/*==============================================================*/
create table STATUS 
(
   ID_STATUS            int                            not null,
   DISETUJUI            varchar(5)                     null,
   PENDING              varchar(5)                     null,
   TGL_DISETUJUI        datetime                       null,
   NOTES                long varchar                   null,
   constraint pk_STATUS primary key clustered (ID_STATUS)
);

/*==============================================================*/
/* Table: TAHAPAN                                               */
/*==============================================================*/
create table TAHAPAN 
(
   ID_TAHAPAN           int                            not null,
   NAMA_TAHAPAN         varchar(50)                    null,
   constraint pk_TAHAPAN primary key clustered (ID_TAHAPAN)
);

/*==============================================================*/
/* Table: UPDATE_PENYELIA                                       */
/*==============================================================*/
create table UPDATE_PENYELIA 
(
   ID_PENYELIA          int                            not null,
   PEMEGANG_SKPD        varchar(100)                   null,
   constraint pk_UPDATE_PENYELIA primary key clustered (ID_PENYELIA)
);

/*==============================================================*/
/* Table: setting_penyelia                                       */
/*==============================================================*/
create table setting_penyelia 
(
   id          int                            not null,
   id_pegawai        int                   not null,
   id_skpd        int                   not null,
   created_at           datetime DEFAULT CURRENT_TIMESTAMP,
   updated_at           datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   constraint pk_setting_penyelia primary key clustered (id)
);

ALTER TABLE `pegawai` MODIFY `id_pegawai` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `jenis_kegiatan` MODIFY `id_jenis_kegiatan` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `histori` MODIFY `id_histori` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `skpd` MODIFY `id_skpd` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `otoritas` MODIFY `id_otoritas` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `pengajuan` MODIFY `id_pengajuan` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `status` MODIFY `id_status` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `tahapan` MODIFY `id_tahapan` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `update_penyelia` MODIFY `id_penyelia` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `setting_penyelia` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

/* DUMP otoritas DATA */
INSERT INTO otoritas (NAMA_otoritas) VALUES('KPA');
INSERT INTO otoritas (NAMA_otoritas) VALUES('PA');
INSERT INTO otoritas (NAMA_otoritas) VALUES('SUPERADMIN');
INSERT INTO otoritas (NAMA_otoritas) VALUES('PENYELIA');

/* DUMP USER DATA */
/* INSERT INTO pegawai (ID_otoritas, nip_pegawai, NAMA_pegawai, jabatan_sebelum, skpd_sebelum, password) VALUES(4, '1', 'Agit Anggriawan', 'Im Admin', 'Super Admin', '123');
INSERT INTO pegawai (ID_otoritas, nip_pegawai, NAMA_pegawai, jabatan_sebelum, skpd_sebelum, password) VALUES(4, '2', 'Yance Safitri S.Ked', '', 'Penyelia', '123');
INSERT INTO pegawai (ID_otoritas, nip_pegawai, NAMA_pegawai, jabatan_sebelum, skpd_sebelum, password) VALUES(4, '3', 'Candrakanta Samosir', '', 'Penyelia', '123');
INSERT INTO pegawai (ID_otoritas, nip_pegawai, NAMA_pegawai, jabatan_sebelum, skpd_sebelum, password) VALUES(4, '4', 'Putri Rahayu', '', 'Penyelia', '123');
INSERT INTO pegawai (ID_otoritas, nip_pegawai, NAMA_pegawai, jabatan_sebelum, skpd_sebelum, password) VALUES(4, '5', 'Zulfa Utami M.Kom.', '', 'Penyelia', '123');
INSERT INTO pegawai (ID_otoritas, nip_pegawai, NAMA_pegawai, jabatan_sebelum, skpd_sebelum, password) VALUES(4, '6', 'Gandi Firgantoro', '', 'Penyelia', '123');
INSERT INTO pegawai (ID_otoritas, nip_pegawai, NAMA_pegawai, jabatan_sebelum, skpd_sebelum, password) VALUES(4, '7', 'Ika Usada', '', 'Penyelia', '123');
INSERT INTO pegawai (ID_otoritas, nip_pegawai, NAMA_pegawai, jabatan_sebelum, skpd_sebelum, password) VALUES(4, '8', 'Emong Hutapea', '', 'Penyelia', '123');
INSERT INTO pegawai (ID_otoritas, nip_pegawai, NAMA_pegawai, jabatan_sebelum, skpd_sebelum, password) VALUES(4, '9', 'Dirja Zulkarnain', '', 'Penyelia', '123');
INSERT INTO pegawai (ID_otoritas, nip_pegawai, NAMA_pegawai, jabatan_sebelum, skpd_sebelum, password) VALUES(4, '10', 'Teddy Ardianto', '', 'Penyelia', '123');
INSERT INTO pegawai (ID_otoritas, nip_pegawai, NAMA_pegawai, jabatan_sebelum, skpd_sebelum, password) VALUES(4, '11', 'Vero Asman Nababan S.T.', '', 'Penyelia', '123'); */
INSERT INTO `pegawai` (`id_pegawai`, `id_otoritas`, `nip_pegawai`, `nama_pegawai`, `jabatan_sebelum`, `skpd_sebelum`, `password`) VALUES
(1, '["Super Admin"]', '13410100068', 'Galih Okta Siwi', 'Kepala Bidang Perencanaan', 'Dinas Pendidikan', '123456'),
(2, '["KPA"]', '13410100069', 'Norman Kamaru', 'Kepala Bidang Prasarana', 'Dinas Kesehatan', '123456'),
(3, '["PA"]', '13410100070', 'Sofyan', 'Kepala Bidang Prasarana', 'Dinas Kesehatan', '123456'),
(4, '["Penyelia"]', '13410100071', 'Leonardo', 'Kepala Bidang Perencanaan', 'Dinas Pemadam Kebakaran', '123456'),
(5, '["KPA", "Penyelia"]', '13410100072', 'Vincent', 'Kepala Bidang Perencanaan', 'Badan Perencanaan Pembangunan', '123456');

/* DUMP SKPD DATA */
/* INSERT INTO SKPD (NAMA_SKPD) VALUES('Dinas Kesehatan');
INSERT INTO SKPD (NAMA_SKPD) VALUES('Dinas Sosial');
INSERT INTO SKPD (NAMA_SKPD) VALUES('Dinas Pendidikan');
INSERT INTO SKPD (NAMA_SKPD) VALUES('Dinas Perhubungan dan Lalu Lintas Angkutan Jalan');
INSERT INTO SKPD (NAMA_SKPD) VALUES('Dinas Komunikasi dan Informatika');
INSERT INTO SKPD (NAMA_SKPD) VALUES('Dinas Tenaga Kerja dan Transmigrasi');
INSERT INTO SKPD (NAMA_SKPD) VALUES('Dinas Kebudayaan dan Pariwisata');
INSERT INTO SKPD (NAMA_SKPD) VALUES('Dinas Koperasi dan Usaha, Mikro, Kecil, Menengah(UMKM)');
INSERT INTO SKPD (NAMA_SKPD) VALUES('Dinas Kepemudaan dan Keolahragaan');
INSERT INTO SKPD (NAMA_SKPD) VALUES('Dinas Pekerjaan Umum Bina Marga'); */
INSERT INTO `skpd` (`ID_SKPD`, `NAMA_SKPD`) VALUES
(1010100, 'Dinas Pendidikan'),
(1020100, 'Dinas Kesehatan'),
(1020200, 'RSUD dr. Mohamad Soewandhie'),
(1020300, 'RSUD Bhakti Dharma Husada'),
(1030100, 'Dinas Pekerjaan Umum Bina Marga dan Pematusan'),
(1040100, 'Dinas Pemadam Kebakaran'),
(1040200, 'Dinas Pengelolaan Bangunan dan Tanah'),
(1050100, 'Dinas Perumahan Rakyat dan Kawasan Permukiman, Cipta Karya dan Tata Ruang'),
(1060100, 'Badan Perencanaan Pembangunan'),
(1070100, 'Dinas Perhubungan'),
(1080100, 'Dinas Lingkungan Hidup'),
(1080200, 'Dinas Kebersihan dan Ruang Terbuka Hijau'),
(1100100, 'Dinas Kependudukan dan Pencatatan Sipil'),
(1120100, 'Dinas Pengendalian Penduduk, Pemberdayaan Perempuan dan Perlindungan Anak'),
(1130100, 'Dinas Sosial'),
(1140100, 'Dinas Tenaga Kerja'),
(1150100, 'Dinas Koperasi dan Usaha Mikro'),
(1160100, 'Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu'),
(1170100, 'Dinas Kebudayaan dan Pariwisata'),
(1180100, 'Dinas Kepemudaan dan Olah Raga'),
(1190100, 'Badan Kesatuan Bangsa, Politik dan Perlindungan Masyarakat'),
(1190200, 'Satuan Polisi Pamong Praja'),
(1190300, 'Badan Penanggulangan Bencana dan Perlindungan Masyarakat'),
(1200100, 'Dewan Perwakilan Rakyat Daerah'),
(1200200, 'Kepala Daerah dan Wakil Kepala Daerah'),
(1200301, 'Bagian Administrasi Pemerintahan dan Otonomi Daerah'),
(1200302, 'Bagian Hukum'),
(1200303, 'Bagian Organisasi'),
(1200304, 'Bagian Administrasi Kerjasama'),
(1200305, 'Bagian Administrasi Pembangunan'),
(1200306, 'Bagian Administrasi Perekonomian dan Usaha Daerah'),
(1200307, 'Bagian Administrasi Kesejahteraan Rakyat'),
(1200308, 'Bagian Umum dan Protokol'),
(1200309, 'Bagian Layanan Pengadaan dan Pengelolaan Aset'),
(1200310, 'Bagian Hubungan Masyarakat'),
(1200400, 'Sekretariat DPRD'),
(1200500, 'Badan Pengelolaan Keuangan dan Pajak Daerah'),
(1200700, 'Inspektorat'),
(1200901, 'Kecamatan Genteng'),
(1200902, 'Kecamatan Simokerto'),
(1200903, 'Kecamatan Tegalsari'),
(1200904, 'Kecamatan Bubutan'),
(1200905, 'Kecamatan Kenjeran'),
(1200906, 'Kecamatan Pabean Cantian'),
(1200907, 'Kecamatan Semampir'),
(1200908, 'Kecamatan Krembangan'),
(1200909, 'Kecamatan Tambaksari'),
(1200910, 'Kecamatan Gubeng'),
(1200911, 'Kecamatan Rungkut'),
(1200912, 'Kecamatan Gunung Anyar'),
(1200913, 'Kecamatan Tenggilis Mejoyo'),
(1200914, 'Kecamatan Sukolilo'),
(1200915, 'Kecamatan Mulyorejo'),
(1200916, 'Kecamatan Wonokromo'),
(1200917, 'Kecamatan Karangpilang'),
(1200918, 'Kecamatan Dukuh Pakis'),
(1200919, 'Kecamatan Gayungan'),
(1200920, 'Kecamatan Jambangan'),
(1200921, 'Kecamatan Wonocolo'),
(1200922, 'Kecamatan Sawahan'),
(1200923, 'Kecamatan Wiyung'),
(1200924, 'Kecamatan Tandes'),
(1200925, 'Kecamatan Asemrowo'),
(1200926, 'Kecamatan Sukomanunggal'),
(1200927, 'Kecamatan Benowo'),
(1200928, 'Kecamatan Lakarsantri'),
(1200929, 'Kecamatan Sambikerep'),
(1200930, 'Kecamatan Pakal'),
(1200931, 'Kecamatan Bulak'),
(1201200, 'Badan Kepegawaian dan Diklat'),
(1250100, 'Dinas Komunikasi dan Informatika'),
(1260100, 'Dinas Perpustakaan dan Kearsipan'),
(2010100, 'Dinas Ketahanan Pangan dan Pertanian'),
(2060100, 'Dinas Perdagangan');

/* DUMP JENIS KEGIATAN */
INSERT INTO `jenis_kegiatan` (`ID_jenis_kegiatan`, `KODE_KEGIATAN`, `NAMA_KEGIATAN`) VALUES
(1, '120005000', 'Belanja Tidak Langsung'),
(2, '21204010001', 'Pengelolaan Disiplin pegawai'),
(3, '22201010002', 'Pelaksanaan Assesment Centre'),
(4, '22201010003', 'Penataan Administrasi Kepegawaian'),
(5, '22201010004', 'Pendidikan dan Pelatihan Kompetensi Fungsional dan'),
(6, '22201010005', 'Pendidikan dan Pelatihan Kompetensi Manajerial'),
(7, '22201010007', 'Pendidikan dan Pelatihan Kompetensi Teknis'),
(8, '22201010008', 'Penempatan pegawai, Promosi dan Rotasi Jabatan'),
(9, '22201010009', 'Pengelolaan Data pegawai'),
(10, '22201010010', 'Pengembangan Wawasan Aparatur'),
(11, '22201010011', 'Penilaian Kinerja pegawai'),
(12, '22201010012', 'Penyiapan Materi Diklat Pembelajaran (e-learning)'),
(13, '22201010013', 'Seleksi Penerimaan Calon PNS'),
(14, '22201010014', 'Pelayanan Administrasi Kepegawaian'),
(15, '22202010002', 'Penyediaan Barang dan Jasa Perkantoran Perangkat D'),
(16, '22202020002', 'Pemeliharaan dan Pengadaan Sarana Perkantoran');

/* DUMP TAHAPAN DATA */
INSERT INTO `tahapan` (`ID_TAHAPAN`, `NAMA_TAHAPAN`) VALUES
(1, 'R-APBD 2 (Buku Biru Provinsi)'),
(2, 'APBD (Murni)'),
(3, 'Revisi 1'),
(4, 'Revisi 2'),
(5, 'Revisi 3'),
(6, 'Revisi 4'),
(7, 'Revisi 5'),
(8, 'Penyesuaian Komponen Belanja 1'),
(9, 'Revisi 6');

/* SET FOREIGN KEY */
-- ALTER TABLE `pegawai` ADD CONSTRAINT `pegawaiotoritas` FOREIGN KEY (`id_otoritas`) REFERENCES `otoritas` (`id_otoritas`);
ALTER TABLE `setting_penyelia` ADD CONSTRAINT `penyelia_pegawai` FOREIGN KEY (`id_pegawai`) REFERENCES `pegawai` (`id_pegawai`);
ALTER TABLE `setting_penyelia` ADD CONSTRAINT `penyelia_skpd` FOREIGN KEY (`id_skpd`) REFERENCES `skpd` (`id_skpd`);
