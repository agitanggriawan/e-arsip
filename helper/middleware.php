<?php
  include_once './config/connect.php';
  class Middleware {
    function __contruct() {
      session_start();
    }

    function __destruct() {

    }

    function checkTitle($url) {
      // global $connect;
      if ($url === 'pegawai') {
        return 'Pegawai';
      } else if ($url === 'kegiatan') {
        return 'Kegiatan';
      } else if ($url === 'tahapan') {
        return 'Tahapan';
      } else if ($url === 'penyelia') {
        return 'Penyelia';
      } else if ($url === 'skpd') {
        return 'SKPD Pengusul';
      } else if ($url === 'set-penyelia') {
        return 'Set Penyelia';
      } else if ($url === 'monitoring') {
        return 'Monitoring';
      } else if ($url === 'history') {
        return 'History';
      } else if ($url === 'data-skpd') {
        return 'Data SKPD';
      } else {
        return 'Not found';
      }
    }

    function logout($id) {
      global $connect;

      $query = "UPDATE pegawai SET last_login = now(), active = 0 WHERE id_pegawai = $id";
      $exec = mysqli_query($connect, $query);
      if ($exec) {
        return true;
      }

      return false;
    }

    function getPenyelia() {
      global $connect;
      $value = '';
      // $query = "SELECT id_pegawai, nama_pegawai FROM pegawai ORDER BY 2 ASC";
      $query = "SELECT id_pegawai, nama_pegawai FROM `pegawai` WHERE JSON_SEARCH(id_otoritas, 'one', '%Penyelia%') IS NOT NULL ORDER BY 2 ASC";
      $exec = mysqli_query($connect, $query);
      while($k = mysqli_fetch_array($exec)){
        $value .= '<option value='.$k['id_pegawai'].'>'.$k['nama_pegawai'].'</option>';
      }
      return $value;
    }

    function getKegiatan() {
      global $connect;
      $value = '';
      $query = "SELECT id_jenis_kegiatan, kode_kegiatan, nama_kegiatan FROM jenis_kegiatan ORDER BY 2 ASC";
      echo $query;
      $exec = mysqli_query($connect, $query);
      while($k = mysqli_fetch_array($exec)){
        $value .= '<option value='.$k['id_jenis_kegiatan'].'>'.$k['nama_kegiatan'].'</option>';
      }
      return $value;
    }

    function getPenyeliaWithId() {
      global $connect;
      $value = '';
      $query = "SELECT id_pegawai, nama_pegawai, nip_pegawai, skpd_sebelum FROM pegawai ORDER BY 2 ASC";
      $exec = mysqli_query($connect, $query);
      while($k = mysqli_fetch_array($exec)){
        $value .= '<option value='.$k['id_pegawai'].'>'.$k['nama_pegawai'].'</option>';
      }
      return $value;
    }

    function getSkpd() {
      global $connect;
      $value = '';
      $query = "SELECT id_skpd, nama_skpd FROM skpd ORDER BY 2 ASC";
      $exec = mysqli_query($connect, $query);
      while($k = mysqli_fetch_array($exec)){
        $value .= '<option value='.$k['id_skpd'].'>'.$k['nama_skpd'].'</option>';
      }
      return $value;
    }

    function getTahapan() {
      global $connect;
      $value = '';
      $query = "SELECT id_tahapan, nama_tahapan FROM tahapan ORDER BY 2 ASC";
      $exec = mysqli_query($connect, $query);
      while($k = mysqli_fetch_array($exec)){
        $value .= '<option value='.$k['id_tahapan'].'>'.$k['nama_tahapan'].'</option>';
      }
      return $value;
    }

    function getPenyeliaByDinas($nama_skpd) {
      global $connect;
      $value = '';
      /* $query = "SELECT p.id_pegawai, p.nama_pegawai FROM setting_penyelia sp JOIN pegawai p ON p.id_pegawai = sp.id_pegawai WHERE sp.id_skpd = $id_skpd";
      $exec = mysqli_query($connect, $query);
      while($k = mysqli_fetch_array($exec)){
        $value .= '<option value='.$k['id_pegawai'].'>'.$k['nama_pegawai'].'</option>';
      } */
      // $query = "SELECT id_pegawai, nama_pegawai FROM pegawai WHERE skpd_sebelum = '$nama_skpd'";
      $query = "SELECT id_pegawai, nama_pegawai FROM `pegawai` WHERE JSON_EXTRACT(`skpd_sebelum`,CONCAT('$[',JSON_LENGTH(`skpd_sebelum`)-1,']')) = '$nama_skpd'";

      $exec = mysqli_query($connect, $query);
      while($k = mysqli_fetch_array($exec)){
        $value .= '<option value='.$k['id_pegawai'].'>'.$k['nama_pegawai'].'</option>';
      }
      return $value;
    }
  }
?>
