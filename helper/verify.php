<?php
  require '../config/connect.php';
  $username = mysqli_real_escape_string($connect, $_POST['username']);
  $password = mysqli_real_escape_string($connect, $_POST['password']);

  $penyelia = $connect->prepare("SELECT p.id_pegawai, p.nip_pegawai, p.nama_pegawai, p.jabatan_sebelum, p.skpd_sebelum, p.id_otoritas FROM pegawai p JOIN setting_penyelia s ON p.id_pegawai = s.id_pegawai WHERE p.nip_pegawai = ? and p.password = ?");
  $penyelia->bind_param('ss', $username, $password);
  $penyelia->execute();
  $penyelia->store_result();
  $penyelia->bind_result($id_pegawai, $nip, $nama_pegawai, $jabatan_sebelum, $skpd_sebelum, $id_otoritas);

  $skpd = $connect->prepare("SELECT id_skpd, nama_skpd FROM skpd WHERE id_skpd = ? and password = ?");
  $skpd->bind_param('ss', $username, $password);
  $skpd->execute();
  $skpd->store_result();
  $skpd->bind_result($id_skpd, $nama_skpd);


  $admin = $connect->prepare("SELECT id_pegawai, nip_pegawai, nama_pegawai, jabatan_sebelum, skpd_sebelum, id_otoritas FROM pegawai WHERE nip_pegawai = ? and password = ? and JSON_SEARCH(id_otoritas, 'one', '%Super Admin%') IS NOT NULL");
  $admin->bind_param('ss', $username, $password);
  $admin->execute();
  $admin->store_result();
  $admin->bind_result($id_pegawai, $nip, $nama_pegawai, $jabatan_sebelum, $skpd_sebelum, $id_otoritas);


  if ($penyelia->num_rows > 0) {
    session_start();
    $penyelia->fetch();
    $_SESSION['id_pegawai'] = base64_encode($id_pegawai);
    $_SESSION['nip'] = base64_encode($nip);
    $_SESSION['nama_pegawai'] = base64_encode($nama_pegawai);
    $_SESSION['jabatan_sebelum'] = base64_encode($jabatan_sebelum);
    $_SESSION['skpd_sebelum'] = base64_encode($skpd_sebelum);
    $_SESSION['id_otoritas'] = $id_otoritas;
    $penyelia->close();

    $set_user = $connect->prepare("UPDATE pegawai SET active = 1 WHERE id_pegawai = $id_pegawai");
    $set_user->execute();

    if (strpos($id_otoritas, 'KPA') !== false || strpos($id_otoritas, 'PA') !== false) {
      header('Location: ../index?module=skpd');
    } else if (strpos($id_otoritas, 'Penyelia') !== false) {
      header('Location: ../index?module=penyelia');
    } else {
      header('Location: ../index?module=pegawai');
    }
  } else if ($skpd->num_rows > 0) {
    session_start();
    $skpd->fetch();
    $id_otoritas = ["Admin"];
    $_SESSION['id_pegawai'] = base64_encode($id_skpd);
    $_SESSION['nip'] = base64_encode($id_skpd);
    $_SESSION['nama_pegawai'] = base64_encode($nama_skpd);
    $_SESSION['skpd_sebelum'] = '';
    $_SESSION['id_otoritas'] = json_encode($id_otoritas);
    $skpd->close();

    header('Location: ../index?module=skpd');
  } else if ($admin->num_rows > 0) {
    session_start();
    $admin->fetch();
    $id_otoritas = ["Super Admin"];
    $_SESSION['id_pegawai'] = base64_encode($id_pegawai);
    $_SESSION['nip'] = base64_encode($nip);
    $_SESSION['nama_pegawai'] = base64_encode($nama_pegawai);
    $_SESSION['jabatan_sebelum'] = base64_encode($jabatan_sebelum);
    $_SESSION['skpd_sebelum'] = base64_encode($skpd_sebelum);
    $_SESSION['id_otoritas'] = json_encode($id_otoritas);
    $admin->close();

    header('Location: ../index?module=pegawai');
  } else {
    header('Location: ../login?n=f1');
  }
?>