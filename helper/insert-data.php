<?php
include('../config/connect.php');
$result = new \stdClass();

if ($_GET['name'] == "pegawai") {
  $namaLengkap = $_POST['namaLengkap'];
  $nip = $_POST['nip'];
  $skpd = $_POST['skpd'];
  $jabatan = $_POST['jabatan'];
  $password = $_POST['password'];
  $otoritas = $_POST['otoritas'];

  $query = mysqli_query($connect, "INSERT INTO pegawai(id_otoritas, nama_pegawai, nip_pegawai, jabatan_sebelum, skpd_sebelum, password) VALUES('".json_encode($otoritas)."'  ,'$namaLengkap', '$nip', '$jabatan', '$skpd', '$password')");

  if ($query) {
    $result->code = "OK";
  } else {
    $result->code = "FAIL";
    $result->msg = mysqli_error($connect);
  }

  echo json_encode($result);
} else if ($_GET['name'] == "update-pegawai") {
  if(isset($_POST["id"])) {
    $value = mysqli_real_escape_string($connect, $_POST["value"]);
    $query = mysqli_query($connect, "UPDATE pegawai SET ".$_POST["column_name"]."='".$value."' WHERE id_pegawai = '".$_POST["id"]."'");

    if ($query) {
      $result->code = "OK";
    } else {
      $result->code = "FAIL";
      $result->msg = mysqli_error($connect);
    }

    echo json_encode($result);
  }
} else if ($_GET['name'] == "insert-kegiatan") {
  $skpd = $_POST['skpd'];
  $kodeKegiatan = $_POST['kodeKegiatan'];
  $namaKegiatan = $_POST['namaKegiatan'];

  $query = mysqli_query($connect, "INSERT INTO jenis_kegiatan(id_skpd, kode_kegiatan, nama_kegiatan) VALUES($skpd, '$kodeKegiatan', '$namaKegiatan')");

  if ($query) {
    $result->code = "OK";
  } else {
    $result->code = "FAIL";
    $result->msg = mysqli_error($connect);
  }

  echo json_encode($result);
} else if ($_GET['name'] == "update-kegiatan") {
  if(isset($_POST["id"])) {
    $value = mysqli_real_escape_string($connect, $_POST["value"]);
    $query = mysqli_query($connect, "UPDATE jenis_kegiatan SET ".$_POST["column_name"]."='".$value."' WHERE id_jenis_kegiatan = '".$_POST["id"]."'");

    if ($query) {
      $result->code = "OK";
    } else {
      $result->code = "FAIL";
      $result->msg = mysqli_error($connect);
    }

    echo json_encode($result);
  }
} else if ($_GET['name'] == "insert-tahapan") {
  $namaTahapan = $_POST['namaTahapan'];

  $query = mysqli_query($connect, "INSERT INTO tahapan(nama_tahapan) VALUES('$namaTahapan')");

  if ($query) {
    $result->code = "OK";
  } else {
    $result->code = "FAIL";
    $result->msg = mysqli_error($connect);
  }

  echo json_encode($result);
} else if ($_GET['name'] == "update-tahapan") {
  if(isset($_POST["id"])) {
    $value = mysqli_real_escape_string($connect, $_POST["value"]);
    $query = mysqli_query($connect, "UPDATE tahapan SET ".$_POST["column_name"]."='".$value."' WHERE id_tahapan = '".$_POST["id"]."'");

    if ($query) {
      $result->code = "OK";
    } else {
      $result->code = "FAIL";
      $result->msg = mysqli_error($connect);
    }

    echo json_encode($result);
  }
} else if ($_GET['name'] == "insert-skpd") {
  $body = json_decode($_POST['body']);
  $usulanSebelum = $body->usulanSebelum;
  $usulanMenjadi = $body->usulanMenjadi;

  $now = new DateTime();
  $timeStamp = $now->getTimestamp();

  if (isset($_FILES['fileUsulan']) && $_FILES['fileUsulan']['name'] != "") {
    $ext = strtolower(@end(explode('.', $_FILES['fileUsulan']['name'])));
    $name = $timeStamp.'.'.$ext;
    $path = 'assets/docs/'.$name;

    if (!is_dir('../assets/docs')) {
      mkdir('../assets/docs');
    }
    move_uploaded_file($_FILES['fileUsulan']['tmp_name'], '../'.$path);
  }

  $query = mysqli_query($connect, "INSERT INTO pengajuan (usulan_sebelum, usulan_menjadi, tanggal_pengajuan, status, file, note_skpd, note_penyelia) VALUES ('".json_encode($usulanSebelum)."', '".json_encode($usulanMenjadi)."', NOW(), 'Pending', '$path', '', '')");

  if ($query) {
    $result->code = "OK";
  } else {
    $result->code = "FAIL";
    $result->msg = mysqli_error($connect);
  }

  echo json_encode($result);
} else if ($_GET['name'] == "update-note-skpd") {
  if(isset($_POST["id"])) {
    $value = mysqli_real_escape_string($connect, $_POST["value"]);
    $query = mysqli_query($connect, "UPDATE pengajuan SET ".$_POST["column_name"]."='".$value."', status = 'Review' WHERE id_pengajuan = '".$_POST["id"]."'");

    if ($query) {
      $result->code = "OK";
    } else {
      $result->code = "FAIL";
      $result->msg = mysqli_error($connect);
    }

    echo json_encode($result);
  }
} else if ($_GET['name'] == "update-status-skpd") {
  if(isset($_POST["id"])) {
    $query = mysqli_query($connect, "UPDATE pengajuan SET status = 'Verified', tanggal_verifikasi = NOW() WHERE id_pengajuan = '".$_POST["id"]."'");

    if ($query) {
      $result->id = $_POST["id"];
      $result->code = "OK";
    } else {
      $result->code = "FAIL";
      $result->msg = mysqli_error($connect);
    }

    echo json_encode($result);
  }
} else if ($_GET['name'] == "set-penyelia") {
  if(isset($_POST["id_pegawai"])) {
    $id_pegawai = $_POST['id_pegawai'];
    $id_skpd = $_POST['id_skpd'];

    for ($i=0; $i < count($id_skpd); $i++) {
      $query = mysqli_query($connect, "INSERT INTO setting_penyelia (id_pegawai, id_skpd) VALUES ($id_pegawai, $id_skpd[$i])");
    }

    if ($query) {
      $result->code = "OK";
    } else {
      $result->code = "FAIL";
      $result->msg = mysqli_error($connect);
    }

    echo json_encode($result);
  }
} else if ($_GET['name'] == "update-status-pengajuan") {
  if(isset($_POST["id"])) {
    $query = mysqli_query($connect, "UPDATE pengajuan SET deleted = 1 WHERE id_pengajuan = '".$_POST["id"]."'");

    if ($query) {
      $result->code = "OK";
    } else {
      $result->code = "FAIL";
      $result->msg = mysqli_error($connect);
    }

    echo json_encode($result);
  }
}
?>