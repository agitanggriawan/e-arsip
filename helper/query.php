<?php
	session_start();
	include "../config/connect.php";

	if ($_GET['name'] == "petugas") {
		$query = mysqli_query($connect, "SELECT * FROM pegawai ORDER BY 5 ASC");
		$arr = [];
		$i = 0;
		while($data = mysqli_fetch_array($query)){
			$last = json_decode($data[4]);
			$arr[$i]['id'] = $i + 1 . '.';
			$arr[$i]['nip'] = json_decode($data[2]);
			$arr[$i]['nama'] = $data[3];
			// $arr[$i]['nip'] = '<div contenteditable class="updatePegawai" data-id="'.$data[0].'" data-column="nip_pegawai">' . $data[2] . '</div>';
			// $arr[$i]['nama'] = '<div contenteditable class="updatePegawai" data-id="'.$data[0].'" data-column="nama_pegawai">' . $data[3] . '</div>';
			$arr[$i]['jabatan'] = @end($last);
			$arr[$i]['skpd'] = json_decode($data[5]);
			$arr[$i]['role'] = json_decode($data[1]);
			$arr[$i]['aksi'] = $data[0];
			$i++;
		}
		header('Content-Type: application/json');
		echo json_encode($arr);
	} else if ($_GET['name'] == "kegiatan") {
		$query = mysqli_query($connect, "SELECT j.id_jenis_kegiatan, j.kode_kegiatan, s.nama_skpd, j.nama_kegiatan FROM jenis_kegiatan j JOIN skpd s ON s.id_skpd = j.id_skpd");
		$arr = [];
		$i = 0;
		while($data = mysqli_fetch_array($query)){
			$arr[$i]['no'] = $i + 1 . '.';
			$arr[$i]['kode'] = $data[1];
			$arr[$i]['skpd'] = $data[2];
			// $arr[$i]['kode'] = '<div contenteditable class="updateKegiatan" data-id="'.$data[0].'" data-column="kode_kegiatan">' . $data[1] . '</div>';
			$arr[$i]['nama'] = '<div contenteditable class="updateKegiatan" data-id="'.$data[0].'" data-column="nama_kegiatan">' . $data[3] . '</div>';
			$i++;
		}
		header('Content-Type: application/json');
		echo json_encode($arr);
	} else if ($_GET['name'] == "tahapan") {
		$query = mysqli_query($connect, "SELECT * FROM tahapan");
		$arr = [];
		$i = 0;
		while($data = mysqli_fetch_array($query)){
			$arr[$i]['no'] = $i + 1 . '.';
			$arr[$i]['id'] = $data[0];
			$arr[$i]['nama'] = '<div contenteditable style="margin-left: 5px" class="updateTahapan" data-id="'.$data[0].'" data-column="nama_tahapan">' . $data[1] . '</div>';
			$i++;
		}
		header('Content-Type: application/json');
		echo json_encode($arr);
	} else if ($_GET['name'] == "skpd") {
		if (isset($_GET['id'])) {
			$query = mysqli_query($connect, "SELECT * FROM skpd WHERE id_skpd NOT IN (SELECT id_skpd FROM setting_penyelia WHERE id_pegawai = ".$_GET['id'].")");
			$arr = [];
			$i = 0;
			while($data = mysqli_fetch_array($query)){
				$arr[$i]['no'] = $i + 1 . '.';
				$arr[$i]['nama'] = $data[1];
				$arr[$i]['aksi'] = '<p><label><input type="checkbox" class="filled-in checkbox-skpd" id="'.$data[0].'"/><span></span></label></p>';
				$i++;
			}
			header('Content-Type: application/json');
			echo json_encode($arr);
		} else {
			$query = mysqli_query($connect, "SELECT * FROM skpd");
			$arr = [];
			$i = 0;
			while($data = mysqli_fetch_array($query)){
				$arr[$i]['no'] = $i + 1 . '.';
				$arr[$i]['nama'] = $data[1];
				$arr[$i]['aksi'] = '<p><label><input type="checkbox" class="filled-in checkbox-skpd" id="'.$data[0].'"/><span></span></label></p>';
				$i++;
			}
			header('Content-Type: application/json');
			echo json_encode($arr);
		}
	} else if ($_GET['name'] == "usulan-skpd") {
		if (in_array("Super Admin", (json_decode($_SESSION['id_otoritas'])))) {
			$query = mysqli_query($connect, "SELECT * FROM pengajuan WHERE deleted = 0 ORDER BY 1 DESC");
			$arr = [];
			$i = 0;
			while($data = mysqli_fetch_array($query)){
				$usulan_sebelum = json_decode($data[1]);
				$usulan_menjadi = json_decode($data[2]);
				$tgl = new Datetime($data[4]);
				$notes = $data[6] ? $data[6] : 'Klik untuk mengisi catatan';

				$arr[$i]['no'] = $i + 1 . '.';
				$arr[$i]['skpd'] = $usulan_sebelum[0]->skpd;
				$arr[$i]['kode'] = $usulan_sebelum[0]->kegiatan;
				$arr[$i]['tahapan'] = $usulan_sebelum[0]->tahapan;
				$arr[$i]['sebelum'] = $usulan_sebelum;
				$arr[$i]['menjadi'] = $usulan_menjadi;
				$arr[$i]['file'] = $data[8];
				$arr[$i]['tanggal'] = $tgl->format("d-m-Y H:i:s");
				$arr[$i]['status'] = $data[3];
				// $arr[$i]['catatan'] = '<div contenteditable="true" placeholder="Klik untuk mengisi catatan" class="updateNoteSkpd" data-id="'.$data[0].'" data-column="note_penyelia">' . $data[6] . '</div>';
				$arr[$i]['catatan_penyelia'] = $data[6];
				$arr[$i]['catatan_skpd'] = $data[7];
				$arr[$i]['role'] = $_SESSION['id_otoritas'];
				$arr[$i]['aksi'] = $data[0];
				$i++;
			}
			header('Content-Type: application/json');
			echo json_encode($arr);
		} else {
			$qDinas = mysqli_query($connect, "SELECT s.nama_skpd FROM skpd s JOIN setting_penyelia sp ON s.id_skpd = sp.id_skpd WHERE sp.id_pegawai = ".base64_decode($_SESSION['id_pegawai'])." ");
			$arr = [];
			$i = 0;
			while($result = mysqli_fetch_array($qDinas)) {
				$query = mysqli_query($connect, "SELECT * FROM pengajuan WHERE JSON_CONTAINS(usulan_sebelum, JSON_OBJECT('skpd', '".$result['nama_skpd']."')) AND deleted = 0 ORDER BY 1 DESC");
				while($data = mysqli_fetch_array($query)){
					$usulan_sebelum = json_decode($data[1]);
					$usulan_menjadi = json_decode($data[2]);
					$tgl = new Datetime($data[4]);
					$notes = $data[6] ? $data[6] : 'Klik untuk mengisi catatan';

					$arr[$i]['no'] = $i + 1 . '.';
					$arr[$i]['skpd'] = $usulan_sebelum[0]->skpd;
					$arr[$i]['kode'] = $usulan_sebelum[0]->kegiatan;
					$arr[$i]['tahapan'] = $usulan_sebelum[0]->tahapan;
					$arr[$i]['sebelum'] = $usulan_sebelum;
					$arr[$i]['menjadi'] = $usulan_menjadi;
					$arr[$i]['file'] = $data[8];
					$arr[$i]['tanggal'] = $tgl->format("d-m-Y H:i:s");
					$arr[$i]['status'] = $data[3];
					// $arr[$i]['catatan'] = '<div contenteditable="true" placeholder="Klik untuk mengisi catatan" class="updateNoteSkpd" data-id="'.$data[0].'" data-column="note_penyelia">' . $data[6] . '</div>';
					$arr[$i]['catatan_penyelia'] = $data[6];
					$arr[$i]['catatan_skpd'] = $data[7];
					$arr[$i]['role'] = $_SESSION['id_otoritas'];
					$arr[$i]['aksi'] = $data[0];
					$i++;
				}
			}
			header('Content-Type: application/json');
			echo json_encode($arr);
		}
	} else if ($_GET['name'] == "monitoring-skpd") {
		$query = mysqli_query($connect, "SELECT * FROM pengajuan WHERE deleted = 0 ORDER BY 1 DESC");
		$arr = [];
		$i = 0;
		while($data = mysqli_fetch_array($query)){
			$usulan_sebelum = json_decode($data[1]);
			$usulan_menjadi = json_decode($data[2]);
			$tgl_pengajuan = new Datetime($data[4]);
			$tgl_verifikasi = new Datetime($data[5]);
			$notes = $data[6] ? $data[6] : 'Klik untuk mengisi catatan';

			$arr[$i]['no'] = $i + 1 . '.';
			$arr[$i]['skpd'] = $usulan_sebelum[0]->skpd;
			$arr[$i]['kode'] = $usulan_sebelum[0]->kegiatan;
			$arr[$i]['tahapan'] = $usulan_sebelum[0]->tahapan;
			$arr[$i]['sebelum'] = $usulan_sebelum;
			$arr[$i]['menjadi'] = $usulan_menjadi;
			$arr[$i]['file'] = $data[8];
			$arr[$i]['tanggal_pengajuan'] = $tgl_pengajuan->format("d-m-Y H:i:s");
			$arr[$i]['tanggal_verifikasi'] = $tgl_verifikasi->format("d-m-Y H:i:s");
			$arr[$i]['status'] = $data[3];
			$arr[$i]['catatan_penyelia'] = $data[6];
			$arr[$i]['catatan_skpd'] = $data[7];
			$i++;
		}
		header('Content-Type: application/json');
		echo json_encode($arr);
	} else if ($_GET['name'] == "get-skpd-by-id") {
		$id_pegawai = $_GET['id_pegawai'];
		$query = mysqli_query($connect, "SELECT sp.id_skpd, s.nama_skpd FROM setting_penyelia sp JOIN skpd s ON s.id_skpd = sp.id_skpd WHERE id_pegawai = $id_pegawai");

		if (mysqli_num_rows($query) > 0) {
			echo '<option selected disabled>--- Pilih ---</option>';
			while($k = mysqli_fetch_array($query)){
				echo '<option value="'.$k['id_skpd'].'">'.$k['nama_skpd'].'</option>\n';
			}
		} else {
			echo '<option selected disabled>Tidak ada data</option>';
		}

	} else if ($_GET['name'] == "checklist-skpd") {
		$query = mysqli_query($connect, "SELECT * FROM `pegawai` WHERE JSON_SEARCH(id_otoritas, 'one', '%Penyelia%') IS NOT NULL ORDER BY 4 ASC");
		$arr = [];
		$i = 0;
		while($data = mysqli_fetch_array($query)){
			$qDetails = mysqli_query($connect, "SELECT s.nama_skpd FROM setting_penyelia sp JOIN skpd s ON s.id_skpd = sp.id_skpd WHERE sp.id_pegawai = $data[0] ORDER BY 1 ASC");
			$arrSkpd = array();
			while($r = mysqli_fetch_array($qDetails)) {
				array_push($arrSkpd, $r[0]);
			}
			$arr[$i]['no'] = $i + 1 . '.';
			$arr[$i]['nip'] = $data[2];
			$arr[$i]['nama'] = $data[3];
			$arr[$i]['aksi'] = !empty($arrSkpd) ? $arrSkpd : null;
			$i++;
		}
		header('Content-Type: application/json');
		echo json_encode($arr);
	} else if ($_GET['name'] == "get-user-by-id") {
		$id_pegawai = $_GET['id_pegawai'];
		$query = mysqli_query($connect, "SELECT skpd_sebelum, id_otoritas, jabatan_sebelum FROM pegawai WHERE id_pegawai = $id_pegawai");
		$result = new \stdClass();
		if (mysqli_num_rows($query) > 0) {
			$k = mysqli_fetch_array($query);
			$result->skpd = $k[0];
			$result->otoritas = $k[1];
			$result->jabatan = $k[2];
		} else {
			$result->skpd = '-';
			$result->otoritas = '-';
			$result->jabatan = '-';
		}
		echo json_encode($result);
	} else if ($_GET['name'] == "monitoring-skpd-by-dinas") {
		$query = mysqli_query($connect, "SELECT * from pengajuan where JSON_CONTAINS(usulan_sebelum, JSON_OBJECT('skpd', '".base64_decode($_SESSION['nama_pegawai'])."')) AND deleted = 0");
		$arr = [];
		$i = 0;
		while($data = mysqli_fetch_array($query)){
			$usulan_sebelum = json_decode($data[1]);
			$usulan_menjadi = json_decode($data[2]);
			$tgl_pengajuan = new Datetime($data[4]);
			$tgl_verifikasi = new Datetime($data[5]);
			$notes = $data[6] ? $data[6] : 'Klik untuk mengisi catatan';

			$arr[$i]['no'] = $i + 1 . '.';
			$arr[$i]['skpd'] = $usulan_sebelum[0]->skpd;
			$arr[$i]['kode'] = $usulan_sebelum[0]->kegiatan;
			$arr[$i]['tahapan'] = $usulan_sebelum[0]->tahapan;
			$arr[$i]['sebelum'] = $usulan_sebelum;
			$arr[$i]['menjadi'] = $usulan_menjadi;
			$arr[$i]['file'] = $data[8];
			$arr[$i]['tanggal_pengajuan'] = $tgl_pengajuan->format("d-m-Y H:i:s");
			$arr[$i]['tanggal_verifikasi'] = $tgl_verifikasi->format("d-m-Y H:i:s");
			$arr[$i]['status'] = $data[3];
			$arr[$i]['catatan_penyelia'] = $data[6];
			$arr[$i]['catatan_skpd'] = $data[7];
			$arr[$i]['aksi'] = $data[0];
			$i++;
		}
		header('Content-Type: application/json');
		echo json_encode($arr);
	}
?>