<?php
  session_start();
  if(isset($_SESSION['id_pegawai'])) {
  include_once './helper/middleware.php';
  $md = new Middleware();
?>
<!DOCTYPE html>
<html>
  <head>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="css/index.css"  media="screen,projection"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
    <link href="css/datatables/dataTables.materializecss.min.css" type="text/css" rel="stylesheet" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="shortcut icon" href="./assets/img/logo.png" type="image/png">
    <title>E-Arsip Usulan Nama Mutasi Sebelum dan Menjadi</title>
  </head>

  <body>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script type="text/javascript" src="js/index.js"></script>
    <script type="text/javascript" src="js/super-admin/kegiatan.js"></script>
    <script type="text/javascript" src="js/super-admin/tahapan.js"></script>
    <script type="text/javascript" src="js/super-admin/set-penyelia.js"></script>
    <script type="text/javascript" src="js/super-admin/skpd.js"></script>
    <script type="text/javascript" src="js/super-admin/penyelia.js"></script>
    <script type="text/javascript" src="js/super-admin/monitoring.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.0/dist/jquery.validate.js"></script>
    <script type="text/javascript" src="js/datatables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/datatables/dataTables.materializecss.min.js"></script>
    <script type="text/javascript" src="js/datatables/table2csv.min.js"></script>
    <script type="text/javascript" src="js/datatables/editor.min.js"></script>
    <?php include_once './layouts/sidebar.php'; ?>
    <main>
      <body>
        <?php include_once './layouts/content.php'; ?>
      </body>
    </main>
  </body>
</html>
<!-- 
      .--'''''''''--.
    .'      .---.      '.
  /    .-----------.    \
  /        .-----.        \
  |       .-.   .-.       |
  |      /   \ /   \      |
  \    | .-. | .-. |    /
    '-._| | | | | | |_.-'
        | '-' | '-' |
        \___/ \___/
      _.-'  /   \  `-._
    .' _.--|     |--._ '.
    ' _...-|     |-..._ '
          |     |
          '.___.'
            | |
            _| |_
          /\( )/\
          /  ` '  \
        | |     | |
        '-'     '-'
        | |     | |
        | |     | |
        | |-----| |
      .`/  |     | |/`.
      |    |     |    |
      '._.'| .-. |'._.'
            \ | /
            | | |
            | | |
            | | |
          /| | |\
        .'_| | |_`.
        `. | | | .'
      .    /  |  \    .
    /o`.-'  / \  `-.`o\
    /o  o\ .'   `. /o  o\
    `.___.'       `.___.'

-->
<?php
} else {
  header('location:login');
}
?>