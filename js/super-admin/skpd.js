$(document).ready(function() {
  $('.pengusul-skpd-button').click(e => {
    e.preventDefault();
    debugger;
    const usulanSebelum = [
      {
        kegiatan: $($('.select-sebelum')[0])
          .children('option:selected')
          .text(),
        penyelia: $($('.select-penyelia-sebelum'))
          .children('option:selected')
          .text(),
        skpd: $('#skpd').val(),
        tahapan: $($('.select-sebelum')[1])
          .children('option:selected')
          .text(),
        otoritas: $('#otoritasSebelum').val(),
        jabatan: $('#jabatanSebelum').val()
      }
    ];

    const usulanMenjadi = [
      {
        kegiatan: $($('.select-menjadi')[1])
          .children('option:selected')
          .text(),
        penyelia: $($('.select-menjadi')[2])
          .children('option:selected')
          .text(),
        skpd: $($('.select-menjadi')[3])
          .children('option:selected')
          .text(),
        tahapan: $($('.select-menjadi')[4])
          .children('option:selected')
          .text(),
        otoritas: $('#otoritasMenjadi').val(),
        jabatan: $('#jabatanMenjadi').val()
      }
    ];

    const body = {
      usulanSebelum: usulanSebelum,
      usulanMenjadi: usulanMenjadi
    };
    debugger;
    const fileUsulan = $('.file-usulan').prop('files')[0];
    let sentData = new FormData();

    if (fileUsulan) {
      sentData.append('fileUsulan', fileUsulan);
    }
    sentData.append('body', JSON.stringify(body));

    try {
      $.ajax({
        url: './helper/insert-data.php?name=insert-skpd',
        method: 'POST',
        data: sentData,
        contentType: false,
        processData: false,
        success: data => {
          const decode = JSON.parse(data);
          if (decode.code === 'OK') {
            $('.pengusul-skpd-button').prop('disabled', true);
            M.toast({
              html: `Usulan SKPD berhasil disimpan`,
              completeCallback: () => {
                // $('#validateUsulanSebelum').trigger('reset');
                // $('#validateUsulanMenjadi').trigger('reset');
                $('.select-sebelum').select2('val', '--- Pilih ---');
                $('.select-menjadi').select2('val', '--- Pilih ---');
                $('.pengusul-skpd-button').prop('disabled', false);
              },
              classes: 'rounded'
            });
          } else {
            M.toast({
              html: `Terjadi kesalahan saat menyimpan`,
              classes: 'rounded'
            });
            $('.pengusul-skpd-button').prop('disabled', false);
            console.log('-->', decode.msg);
          }
        },
        error: error => {
          console.log('Error', error);
          $('.kegiatan-button').prop('disabled', false);
        }
      });
    } catch (error) {
      console.log('Error', error);
    }
  });

  $('.select-skpd-penyelia').change(function() {
    const id_pegawai = $('.select-skpd-penyelia').val();

    $.ajax({
      url: './helper/query.php?name=get-skpd-by-id',
      data: { id_pegawai: id_pegawai },
      cache: false,
      success: function(msg) {
        $('.select-skpd-by-id').html(msg);
        debugger;
      },
      error: error => {
        console.log('Error', error);
      }
    });
  });

  $('.select-penyelia-sebelum').change(function() {
    const id_pegawai = $('.select-penyelia-sebelum').val();

    $.ajax({
      url: './helper/query.php?name=get-user-by-id',
      data: { id_pegawai: id_pegawai },
      cache: false,
      success: function(msg) {
        const data = JSON.parse(msg);
        const formatData = JSON.parse(data.skpd);
        const formatOtoritas = JSON.parse(data.otoritas);
        const formatJabatan = JSON.parse(data.jabatan);
        $('#skpd').val(formatData.slice(-1).pop());
        $('#otoritasSebelum').val(formatOtoritas.slice(-1).pop());
        $('#jabatanSebelum').val(formatJabatan.slice(-1).pop());
      },
      error: error => {
        console.log('Error', error);
      }
    });
  });
});
