$(document).ready(function() {
  $('#table-skpd').DataTable({
    processing: true,
    ajax: {
      url: './helper/query.php?name=skpd',
      type: 'GET',
      dataSrc: ''
    },
    columns: [{ data: 'no' }, { data: 'nama' }, { data: 'aksi' }],
    columnDefs: [
      { width: '10%', targets: 0 },
      { width: '80%', targets: 1 },
      { width: '10%', targets: 2 }
    ]
  });

  $('#set-penyelia').prop('disabled', true);
  $('.select-penyelia').change(e => {
    $('#table-skpd')
      .dataTable()
      .fnDestroy();
    const id_pegawai = $($('.select-penyelia')[0])
      .children('option:selected')
      .val();
    $('#table-skpd').DataTable({
      processing: true,
      ajax: {
        url: `./helper/query.php?name=skpd&id=${id_pegawai}`,
        type: 'GET',
        dataSrc: ''
      },
      columns: [{ data: 'no' }, { data: 'nama' }, { data: 'aksi' }],
      columnDefs: [
        { width: '10%', targets: 0 },
        { width: '80%', targets: 1 },
        { width: '10%', targets: 2 }
      ]
    });
    $('#table-skpd')
      .DataTable()
      .ajax.reload(null, false);
  });
  var index = 0;
  let id = [];
  $(document).on('change', '.checkbox-skpd', function(e) {
    if (this.checked) {
      $(this).attr('checked', true);
      index += 1;
      id.push(this.id);
      window.localStorage.setItem('id_skpd', JSON.stringify(id));
    } else {
      $(this).attr('checked', false);
      index -= 1;
      var idx = id.indexOf(this.id);

      if (idx > -1) {
        id.splice(idx, 1);
      }
      window.localStorage.removeItem('id_skpd');
      window.localStorage.setItem('id_skpd', JSON.stringify(id));
    }

    index === 0
      ? $('#set-penyelia').prop('disabled', true)
      : $('#set-penyelia').prop('disabled', false);
  });

  $('#set-penyelia').click(e => {
    e.preventDefault();

    debugger;
    const id_skpd = JSON.parse(localStorage.getItem('id_skpd'));
    const id_pegawai = $($('.select-penyelia')[0])
      .children('option:selected')
      .val();

    const myData = {
      id_pegawai,
      id_skpd
    };

    $.ajax({
      url: './helper/insert-data.php?name=set-penyelia',
      method: 'POST',
      data: myData,
      success: data => {
        const decode = JSON.parse(data);
        if (decode.code === 'OK') {
          $('#set-penyelia').prop('disabled', true);
          M.toast({
            html: `Setting penyelia berhasil disimpan`,
            completeCallback: () => {
              // $('#validateProfile').trigger('reset');
              setTimeout(function() {
                $('#table-skpd')
                  .dataTable()
                  .fnDestroy();
                const id_pegawai = $($('.select-penyelia')[0])
                  .children('option:selected')
                  .val();
                $('#table-skpd').DataTable({
                  processing: true,
                  ajax: {
                    url: `./helper/query.php?name=skpd&id=${id_pegawai}`,
                    type: 'GET',
                    dataSrc: ''
                  },
                  columns: [{ data: 'no' }, { data: 'nama' }, { data: 'aksi' }],
                  columnDefs: [
                    { width: '10%', targets: 0 },
                    { width: '80%', targets: 1 },
                    { width: '10%', targets: 2 }
                  ]
                });
                $('#table-skpd')
                  .DataTable()
                  .ajax.reload(null, false);

                $('#table-checklist-skpd')
                  .dataTable()
                  .fnDestroy();
                $('#table-checklist-skpd').dataTable({
                  processing: true,
                  ajax: {
                    url: './helper/query.php?name=checklist-skpd',
                    type: 'GET',
                    dataSrc: ''
                  },
                  autoWidth: false,
                  columnDefs: [
                    { width: '10px', targets: 0 },
                    { width: '40px', targets: 1 },
                    { width: '100px', targets: 2 },
                    { width: '70px', targets: 3 }
                  ],
                  columns: [
                    { data: 'no' },
                    { data: 'nip' },
                    { data: 'nama' },
                    {
                      mData: 'aksi',
                      mRender: (data, type, row) => {
                        if (data) {
                          return data.join('<br>');
                        }
                        return '<b>Tidak ada data</b>';
                      }
                    }
                  ]
                });
                $('#table-checklist-skpd')
                  .DataTable()
                  .ajax.reload(null, false);
              }, 1000);

              $('#set-penyelia').prop('disabled', false);
            },
            classes: 'rounded'
          });
        } else {
          M.toast({
            html: `Terjadi kesalahan saat menyimpan`,
            classes: 'rounded'
          });
          $('#set-penyelia').prop('disabled', false);
          // console.log('-->', decode.msg);
        }
      },
      error: error => {
        console.log('Error', error);
        $('#set-penyelia').prop('disabled', false);
      }
    });

    // var data = table.rows().data();
    // data.each(function(value, index) {
    //   console.log(`For index ${index}, data value is ${JSON.stringify(value)}`);
    //   // console.log(`Status: ${$('.checkbox-skpd')[index]}`);
    // });
  });

  $('#table-checklist-skpd').dataTable({
    processing: true,
    ajax: {
      url: './helper/query.php?name=checklist-skpd',
      type: 'GET',
      dataSrc: ''
    },
    autoWidth: false,
    columnDefs: [
      { width: '10px', targets: 0 },
      { width: '40px', targets: 1 },
      { width: '100px', targets: 2 },
      { width: '70px', targets: 3 }
    ],
    columns: [
      { data: 'no' },
      { data: 'nip' },
      { data: 'nama' },
      {
        mData: 'aksi',
        mRender: (data, type, row) => {
          if (data) {
            return data.join('<br>');
          }
          return '<b>Tidak ada data</b>';
        }
      }
    ]
  });
});
