$(document).ready(function() {
  $('#validateTahapan').validate({
    rules: {
      namaTahapan: {
        required: true
      }
    },
    messages: {
      namaTahapan: {
        required: 'Masukkan nama tahapan'
      }
    },
    errorElement: 'div',
    errorPlacement: function(error, element) {
      var placement = $(element).data('error');
      if (placement) {
        $(placement).append(error);
      } else {
        error.insertAfter(element);
      }
    },
    submitHandler: function(form, e) {
      e.preventDefault();

      var namaTahapan = $('#namaTahapan').val();

      const myData = {
        namaTahapan
      };

      $.ajax({
        url: './helper/insert-data.php?name=insert-tahapan',
        method: 'POST',
        data: myData,
        success: data => {
          const decode = JSON.parse(data);
          if (decode.code === 'OK') {
            $('.tahapan-button').prop('disabled', true);
            M.toast({
              html: `Tahapan berhasil disimpan`,
              completeCallback: () => {
                $('#validateTahapan').trigger('reset');
                setTimeout(function() {
                  $('#table-tahapan')
                    .DataTable()
                    .ajax.reload(null, false);
                }, 1000);
                $('.tahapan-button').prop('disabled', false);
              },
              classes: 'rounded'
            });
          } else {
            M.toast({
              html: `Terjadi kesalahan saat menyimpan`,
              classes: 'rounded'
            });
            $('.tahapan-button').prop('disabled', false);
            console.log('-->', decode.msg);
          }
        },
        error: error => {
          console.log('Error', error);
          $('.tahapan-button').prop('disabled', false);
        }
      });
    }
  });

  $('#table-tahapan').dataTable({
    processing: true,
    ajax: {
      url: './helper/query.php?name=tahapan',
      type: 'GET',
      dataSrc: ''
    },
    columns: [
      { data: 'no' },
      { data: 'id' },
      { data: 'nama' }
      // { visible: false, targets: 1 }
    ]
  });

  function updateTahapan(id, column_name, value) {
    $.ajax({
      url: './helper/insert-data.php?name=update-tahapan',
      method: 'POST',
      data: { id: id, column_name: column_name, value: value },
      success: function(data) {
        const decode = JSON.parse(data);
        if (decode.code === 'OK') {
          M.toast({
            html: 'Data berhasil diperbarui',
            completeCallback: () => {
              setTimeout(function() {
                $('#table-tahapan')
                  .DataTable()
                  .ajax.reload(null, false);
              }, 1000);
            },
            classes: 'rounded'
          });
        } else {
          M.toast({
            html: `Terjadi kesalahan saat memperbarui data`,
            classes: 'rounded'
          });
          console.log('-->', decode.msg);
        }
      }
    });
    setInterval(function() {
      $('#alert_message').html('');
    }, 5000);
  }

  $(document).on('blur', '.updateTahapan', function() {
    var id = $(this).data('id');
    var column_name = $(this).data('column');
    var value = $(this).text();
    debugger;
    updateTahapan(id, column_name, value);
  });
});
