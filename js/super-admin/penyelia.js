$(document).ready(function() {
  $('#table-usulan-skpd').dataTable({
    processing: true,
    ajax: {
      url: './helper/query.php?name=usulan-skpd',
      type: 'GET',
      dataSrc: ''
    },
    columns: [
      { data: 'no' },
      { data: 'skpd' },
      { data: 'kode' },
      { data: 'tahapan' },
      {
        mData: 'sebelum',
        mRender: data => {
          return `1. ${data[0].skpd}<br>2. ${data[0].penyelia}<br>3. ${
            data[0].tahapan
          }<br>4. ${data[0].otoritas}<br>5. ${data[0].jabatan}`;
        }
      },
      {
        mData: 'menjadi',
        mRender: data => {
          return `1. ${data[0].skpd}<br>2. ${data[0].penyelia}<br>3. ${
            data[0].tahapan
          }<br>4. ${data[0].otoritas}<br>5. ${data[0].jabatan}`;
        }
      },
      {
        mData: 'file',
        mRender: data => {
          if (!data) {
            return `<a href="${data}" disabled target="_blank" class="btn blue accent-4"><i class="material-icons">file_download</i></a>`;
          }
          return `<a href="${data}" target="_blank" class="btn blue accent-4"><i class="material-icons">file_download</i></a>`;
        }
      },
      { data: 'tanggal' },
      { data: 'status' },
      {
        mData: 'catatan_penyelia',
        mRender: (data, type, row) => {
          if (row.role.includes('KPA') || row.role.includes('PA')) {
            return data;
          } else {
            return row.status === 'Verified'
              ? data
              : `<div contenteditable="true" placeholder="Klik untuk mengisi catatan" class="updateNotePenyelia" data-id="${
                  row.aksi
                }" data-column="note_penyelia">${data}</div>`;
          }
        }
      },
      {
        mData: 'catatan_skpd',
        mRender: (data, type, row) => {
          return data;
        }
      },
      {
        mData: 'aksi',
        mRender: (data, type, row) => {
          return row.status === 'Pending' || row.status === 'Review'
            ? `<button class="btn light-blue" id="btn-verified-${
                row.aksi
              }" onclick="updateStatusSkpd(${data})">Verifikasi</button>`
            : `<button class="btn light-blue" disabled>Verifikasi</button>`;
        }
      }
    ],
    columnDefs: [{ visible: false, targets: 1 }],
    order: [[1, 'asc']],
    drawCallback: function(settings) {
      var api = this.api();
      var rows = api
        .rows({
          page: 'current'
        })
        .nodes();
      var last = null;

      api
        .column(1, {
          page: 'current'
        })
        .data()
        .each(function(group, i) {
          if (last !== group) {
            $(rows)
              .eq(i)
              .before(
                '<tr class="group"><td colspan="11">' + group + '</td></tr>'
              );

            last = group;
          }
        });
    }
  });

  updateStatusSkpd = id => {
    $(`#btn-verified-${id}`).prop('disabled', true);
    $.ajax({
      url: './helper/insert-data.php?name=update-status-skpd',
      method: 'POST',
      data: { id: id },
      success: function(data) {
        const decode = JSON.parse(data);
        if (decode.code === 'OK') {
          M.toast({
            html: 'Status berhasil diperbarui',
            completeCallback: () => {
              setTimeout(function() {
                $('#table-usulan-skpd')
                  .DataTable()
                  .ajax.reload(null, false);
              }, 1000);
            },
            classes: 'rounded'
          });
        } else {
          M.toast({
            html: `Terjadi kesalahan saat memperbarui status`,
            classes: 'rounded'
          });
          console.log('-->', decode.msg);
        }
      }
    });
  };

  updateNote = (id, column_name, value) => {
    $.ajax({
      url: './helper/insert-data.php?name=update-note-skpd',
      method: 'POST',
      data: { id: id, column_name: column_name, value: value },
      success: function(data) {
        const decode = JSON.parse(data);
        if (decode.code === 'OK') {
          M.toast({
            html: 'Catatan berhasil diperbarui',
            completeCallback: () => {
              setTimeout(function() {
                $('#table-usulan-skpd')
                  .DataTable()
                  .ajax.reload(null, false);
              }, 1000);
            },
            classes: 'rounded'
          });
        } else {
          M.toast({
            html: `Terjadi kesalahan saat memperbarui catatan`,
            classes: 'rounded'
          });
          console.log('-->', decode.msg);
        }
      }
    });
  };

  $(document).on('blur', '.updateNotePenyelia', function() {
    var id = $(this).data('id');
    var column_name = $(this).data('column');
    var value = this.innerText.replace(/(?:\r\n|\r|\n)/g, '<br>');
    value !== '' ? updateNote(id, column_name, value) : null;
  });

  $(document).on('blur', '.updateNoteSkpd', function() {
    var id = $(this).data('id');
    var column_name = $(this).data('column');
    var value = this.innerText.replace(/(?:\r\n|\r|\n)/g, '<br>');
    value !== '' ? updateNote(id, column_name, value) : null;
  });
});
