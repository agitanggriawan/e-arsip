$(document).ready(function() {
  var table = $('#table-monitoring-skpd').DataTable({
    processing: true,
    ajax: {
      url: './helper/query.php?name=monitoring-skpd',
      type: 'GET',
      dataSrc: ''
    },
    columns: [
      { data: 'no' },
      { data: 'skpd' },
      { data: 'kode' },
      { data: 'tahapan' },
      {
        mData: 'sebelum',
        mRender: data => {
          return `1. ${data[0].skpd}<br>2. ${data[0].penyelia}<br>3. ${
            data[0].tahapan
          }<br>4. ${data[0].otoritas}`;
        }
      },
      {
        mData: 'menjadi',
        mRender: data => {
          return `1. ${data[0].skpd}<br>2. ${data[0].penyelia}<br>3. ${
            data[0].tahapan
          }<br>4. ${data[0].otoritas}`;
        }
      },
      { data: 'tanggal_pengajuan' },
      { data: 'tanggal_verifikasi' },
      { data: 'status' },
      { data: 'catatan_penyelia' },
      { data: 'catatan_skpd' },
      {
        mData: 'file',
        mRender: data => {
          return `<a href="${data}" target="_blank" class="btn blue accent-4"><i class="material-icons">file_download</i></a>`;
        }
      }
    ]
  });

  $.fn.dataTable.ext.search.push(function(settings, data, dataIndex) {
    if (settings.nTable.id === 'table-monitoring-skpd') {
      var penyelia = $('#list-skpd')
        .children('option:selected')
        .text();
      var str = data[3].toLowerCase();

      if (penyelia === '--- Pilih ---' || penyelia === 'Semua') {
        return true;
      } else if (str.includes(penyelia.toLowerCase())) {
        return true;
      }
      return false;
    } else {
      return true;
    }
  });

  $('#min').keyup(function() {
    table.draw();
  });

  $('#list-skpd').change(function() {
    table.draw();
  });

  var MonitoringById = $('#table-monitoring-skpd-by-dinas').DataTable({
    processing: true,
    ajax: {
      url: './helper/query.php?name=monitoring-skpd-by-dinas',
      type: 'GET',
      dataSrc: ''
    },
    columns: [
      { data: 'no' },
      { data: 'skpd' },
      { data: 'kode' },
      { data: 'tahapan' },
      {
        mData: 'sebelum',
        mRender: data => {
          return `1. ${data[0].skpd}<br>2. ${data[0].penyelia}<br>3. ${
            data[0].tahapan
          }<br>4. ${data[0].otoritas}<br>5. ${data[0].jabatan}`;
        }
      },
      {
        mData: 'menjadi',
        mRender: data => {
          return `1. ${data[0].skpd}<br>2. ${data[0].penyelia}<br>3. ${
            data[0].tahapan
          }<br>4. ${data[0].otoritas}<br>5. ${data[0].jabatan}`;
        }
      },
      { data: 'tanggal_pengajuan' },
      { data: 'tanggal_verifikasi' },
      { data: 'status' },
      { data: 'catatan_penyelia' },
      {
        mData: 'catatan_skpd',
        mRender: (data, type, row) => {
          console.log('RO', row);
          return row.status === 'Verified'
            ? data
            : `<div contenteditable="true" placeholder="Klik untuk mengisi catatan" class="noteSKPD" data-id="${
                row.aksi
              }" data-column="note_skpd">${data}</div>`;
        }
      },
      {
        mData: 'file',
        mRender: (data, type, row) => {
          debugger;
          let btn = data
            ? `<a href="${data}" target="_blank" class="btn blue accent-4"><i class="material-icons">file_download</i></a>`
            : `<a href="${data}" disabled target="_blank" class="btn blue accent-4"><i class="material-icons">file_download</i></a>`;

          btn +=
            row.status !== 'Verified'
              ? `&emsp;<button class="btn red waves-effect" title="Hapus" id="btn-verified-${
                  row.aksi
                }" onclick="deletePengajuan(${
                  row.aksi
                })"><i class="material-icons">delete</i></button>`
              : null;

          return btn;
        }
      }
    ]
  });

  deletePengajuan = id => {
    // debugger;
    $.ajax({
      url: './helper/insert-data.php?name=update-status-pengajuan',
      method: 'POST',
      data: { id: id },
      success: function(data) {
        const decode = JSON.parse(data);
        if (decode.code === 'OK') {
          M.toast({
            html: 'Pengajuan berhasil dihapus',
            completeCallback: () => {
              setTimeout(function() {
                $('#table-monitoring-skpd-by-dinas')
                  .DataTable()
                  .ajax.reload(null, false);
              }, 1000);
            },
            classes: 'rounded'
          });
        } else {
          M.toast({
            html: `Terjadi kesalahan saat menghapus pengajuan`,
            classes: 'rounded'
          });
          console.log('-->', decode.msg);
        }
      }
    });
  };

  updateNote = (id, column_name, value) => {
    $.ajax({
      url: './helper/insert-data.php?name=update-note-skpd',
      method: 'POST',
      data: { id: id, column_name: column_name, value: value },
      success: function(data) {
        const decode = JSON.parse(data);
        if (decode.code === 'OK') {
          M.toast({
            html: 'Catatan berhasil diperbarui',
            completeCallback: () => {
              setTimeout(function() {
                $('#table-monitoring-skpd-by-dinas')
                  .DataTable()
                  .ajax.reload(null, false);
              }, 1000);
            },
            classes: 'rounded'
          });
        } else {
          M.toast({
            html: `Terjadi kesalahan saat memperbarui catatan`,
            classes: 'rounded'
          });
          console.log('-->', decode.msg);
        }
      }
    });
  };

  updateSKPD = (id, column_name, value) => {
    $.ajax({
      url: './helper/insert-data.php?name=update-note-skpd',
      method: 'POST',
      data: { id: id, column_name: column_name, value: value },
      success: function(data) {
        const decode = JSON.parse(data);
        if (decode.code === 'OK') {
          M.toast({
            html: 'Catatan berhasil diperbarui',
            completeCallback: () => {
              setTimeout(function() {
                $('#table-monitoring-skpd-by-dinas')
                  .DataTable()
                  .ajax.reload(null, false);
              }, 1000);
            },
            classes: 'rounded'
          });
        } else {
          M.toast({
            html: `Terjadi kesalahan saat memperbarui catatan`,
            classes: 'rounded'
          });
          console.log('-->', decode.msg);
        }
      }
    });
  };

  $(document).on('blur', '.noteSKPD', function() {
    var id = $(this).data('id');
    var column_name = $(this).data('column');
    var value = this.innerText.replace(/(?:\r\n|\r|\n)/g, '<br>');
    console.log('==>', id);
    console.log('==>', column_name);
    console.log('==>', value);
    value !== '' ? updateSKPD(id, column_name, value) : null;
  });
});
