<!-- 26a69a  fe #039be5-->
<!DOCTYPE html>
<html lang="en">
<head>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
  <link type="text/css" rel="stylesheet" href="css/index.css"  media="screen,projection"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <link rel="shortcut icon" href="./assets/img/logo.png" type="image/png">
  <title>E-Arsip Usulan Nama Mutasi Sebelum dan Menjadi</title>
</head>
<body>
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="js/materialize.min.js"></script>
  <div class="valign-wrapper">
      <div class="row">
        <div class="card card-login">
          <div class="card-content">
            <img src="./assets/img/logo.png" alt="" class="img-login">
              <div class="title-login">
                <p>E-Arsip Usulan Nama Mutasi</p>
                <p>Sebelum dan Menjadi</p>
              </div>
              <div class="row">
                <?php
                if (isset($_GET['n'])){
                  if($_GET['n'] == "f1") {
                      echo '
                      <script>
                        setTimeout(function(){
                          M.toast({html: "Username atau Password salah", classes: "rounded"});
                        },1000)
                      </script>
                      ';  
                  }
                }
                ?>
                <form action="./helper/verify.php" method="POST">
                  <div class="row">
                    <div class="input-field col s12">
                      <!-- <label for="namaLengkap">Nama Lengkap</label>
                      <input id="namaLengkap" name="namaLengkap" type="text" data-error=".errorNamaLengkap">
                      <div class="errorNamaLengkap errorText"></div> -->
                      <i class="material-icons prefix">account_circle</i>
                      <input id="username" name="username" type="text" class="validate" required="" aria-required="true">
                      <label for="username">Username</label>
                    </div>
                    <div class="input-field col s12">
                      <i class="material-icons prefix">lock</i>
                      <input id="password" name="password" type="password" class="validate" required="" aria-required="true">
                      <label for="password">Password</label>
                    </div>
                    <div class="input-field col s12" style="text-align: center">
                      <button class="btn blue accent-4" type="submit">Login</button>
                    </div>
                  </div>
                </form>
              </div>
          </div>
        </div>
      </div>
  </div>
</body>
</html>